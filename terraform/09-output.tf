output "external_ips" {
  value = {
    for instance in yandex_compute_instance.node :
    instance.name => yandex_compute_instance.node[instance.name].network_interface.0.nat_ip_address
  }
}

resource "local_file" "ansible_hosts" {
  content = templatefile("./template/hosts.tftpl", {
    nodes = {
      for key, values in local.virtual_machines : 
        key => {
          ip = yandex_compute_instance.node[key].network_interface.0.nat_ip_address
        }
    },
    domain_name       = var.domain-name,
    app_user          = var.app-user,
    app_ssh_key_path  = var.app-ssh-key-path,
    balancer_user     = var.balancer-user,
    balancer_ssh_path = var.balancer-ssh-key-path,
  })
  filename = "../inventory/prod/hosts"
}
