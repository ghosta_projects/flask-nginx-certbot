# Flask-app, nginx(loadbalncer) & Certbot

## Инфраструктура

Инфраструктура для проекта разворачивается с помощью terraform. Для этого необходимо заполнить [terraform.tfvars.example](https://gitlab.com/ghosta_projects/flask-nginx-certbot/-/blob/master/terraform/terraform.tfvars.example), переименовать его => **terraform.tfvars** и [backend.conf.example](https://gitlab.com/ghosta_projects/flask-nginx-certbot/-/blob/master/terraform/backend.conf.example), переименовать в => **backend.conf**, настройки можно изменить в [00-vars.tf](https://gitlab.com/ghosta_projects/flask-nginx-certbot/-/blob/master/terraform/00-vars.tf) и выполнить последовательно две команды:
```
$ terraform init -backend-config=backend.conf

$ terraform apply -auto-approve
```
Далее нужно скачать роли для ansible и запустить [playbook](https://gitlab.com/ghosta_projects/flask-nginx-certbot/-/blob/master/playbook.yml) (файл hosts для ansible генерируется с помощью terraform, пример находится в папке [inventory](https://gitlab.com/ghosta_projects/flask-nginx-certbot/-/tree/master/inventory/prod)):
```
$ ansible-galaxy install -r requirements.yml

$ ansible-playbook playbook.yml
```

## Проект

Приложение копируется, собирается в контейнере docker и разворачивается на трех хостах, на четвертом устанавлюваются: 
1. Nginx, в качестве балансировщика нагрузки (режим балансировки по умолчанию - round robin).
2. Certbot получает сертификаты let's encrypt для nginx, а так же следит за их обновлением.
